#pragma once

#include "cocos2d.h"
#include <iostream>

using namespace cocos2d;
using namespace std;

class Pokemon
{
public:
	Pokemon(std::string name, int pv, int atk, int def, int speed, int atkSpec, int defSpec, int precision, int dodge);
	Pokemon(int id, std::string name, int pv, int atk, int def, int speed, int atkSpec, int defSpec, int precision, int dodge);
	~Pokemon();

	// GETTERS / SETTERS
	int getId();
	void setId(int id);
	std::string getName();
	void setName(std::string name);
	int getPv();
	void setPv(int pv);
	int getAtk();
	void setAtk(int atk);
	int getDef();
	void setDef(int def);
	int getSpeed();
	void setSpeed(int speed);
	int getAtkSpec();
	void setAtkSpec(int atkSpec);
	int getDefSpec();
	void setDefSpec(int defSpec);
	int getPrecision();
	void setPrecision(int precision);
	int getDodge();
	void setDodge(int dodge);
	Sprite* getMiniThumb();
	void setMiniThumb(Sprite* sprite);
	Sprite* getMediumThumb();
	void setMediumThumb(Sprite* sprite);
	Sprite* getFightFace();
	void setFightFace(Sprite* sprite);
	Sprite* getFightBack();
	void setFightBack(Sprite* sprite);

protected:

	int id;
	std::string name;
	int pv;
	int atk;
	int def;
	int speed;
	int atkSpec;
	int defSpec;
	int precision; 
	int dodge;
	Sprite* miniThumb;
	Sprite* mediumThumb;
	Sprite* fightFace;
	Sprite* fightBack;
	
};

