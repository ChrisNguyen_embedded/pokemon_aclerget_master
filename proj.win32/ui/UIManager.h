#pragma once

#include "cocos2d.h"

using namespace cocos2d;

#ifndef UI_ZORDER
	#define  UI_ZORDER 50
#endif // !UI_ZORDER

class UIManager : public Node
{

public:

	UIManager();

	static UIManager* create();
	static UIManager* getInstance();

protected:
	static UIManager* instance;

};

