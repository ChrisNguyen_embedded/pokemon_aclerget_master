#include "MapManager.h"
#include "proj.win32/tools/Debug.h"
#include <SimpleAudioEngine.h>


//INIT CONSTANT
std::string MapManager::MAPS_DIRECTORY_PATH = "maps/";
float MapManager::TILE_SIZE = 16.f;
TMXTiledMap * MapManager::mapInfo = nullptr;
vector<struct Collision> MapManager::collisions;
vector<struct Collision> MapManager::fightZone;

MapManager::MapManager()
{
}

MapManager::MapManager(std::string mapName)
{
	this->id = 0;
	this->mapName = mapName;
	this->mapFile = MAPS_DIRECTORY_PATH + mapName;
	this->mapInformation = nullptr;

	this->createMap();
}

MapManager::MapManager(int id, std::string mapName)
{
	this->id = id;
	this->mapName = mapName;
	this->mapFile = MAPS_DIRECTORY_PATH + mapName;
	this->mapInformation = nullptr;

	this->createMap();
}

MapManager::~MapManager()
{
	delete(this->mapInformation);
}

int MapManager::getId()
{
	return this->id;
}

std::string MapManager::getMapName()
{
	return this->mapName;
}

void MapManager::createMap()
{
	auto layer = Director::getInstance()->getRunningScene()->getChildByName("Game");

	if (layer)
	{
		// create a TMX map
		this->mapInformation = TMXTiledMap::create(this->mapFile);

		#pragma region TILE ABOVE CHARACTER

		//add tiles above the player
		auto tiledLayer = this->mapInformation->getLayer("Above");
		tiledLayer->setLocalZOrder(TILE_ABOVE);
		
		#pragma endregion

		#pragma region ADD COLLISION INTO MAP

		//add collision to tiles of "collision" objects
		auto objectGroup = this->mapInformation->getObjectGroup("Collision");
		for (auto object : objectGroup->getObjects())
		{
			auto value = object.asValueMap();

			MapManager::collisions.push_back(
				Collision(value.at("x").asFloat(), value.at("y").asFloat() + 2,
						  value.at("width").asFloat(), value.at("height").asFloat() + 2));

			//DRAW DEBUG RECT
			if (DEBUG_MODE == 1)
			{
				auto rect = DrawNode::create();
				rect->drawRect(
					Vec2(value.at("x").asFloat(), value.at("y").asFloat()),
					Vec2(value.at("x").asFloat() + value.at("width").asFloat(), value.at("y").asFloat() + value.at("height").asFloat()),
					Color4F::RED);

				//we make margin for the rectangle is above the layer "above"
				this->addChild(rect, TILE_ABOVE + 5);
				//log("%f", value.at("x").asFloat());
			}
		}

		#pragma endregion

		#pragma region ADD FIGHTING ZONE INTO MAP

		//add grasses fighting zone
		objectGroup = this->mapInformation->getObjectGroup("Fight_Zone");
		for (auto object : objectGroup->getObjects())
		{
			auto value = object.asValueMap();

			MapManager::fightZone.push_back(
				Collision(value.at("x").asFloat(), value.at("y").asFloat() + 2,
				value.at("width").asFloat(), value.at("height").asFloat() + 2));

			//DRAW DEBUG RECT
			if (DEBUG_MODE == 1)
			{
				auto rect = DrawNode::create();
				rect->drawRect(
					Vec2(value.at("x").asFloat(), value.at("y").asFloat()),
					Vec2(value.at("x").asFloat() + value.at("width").asFloat(), value.at("y").asFloat() + value.at("height").asFloat()),
					Color4F::BLUE);

				//we make margin for the rectangle is above the layer "above"
				this->addChild(rect, TILE_ABOVE + 4);
				//log("%f", value.at("x").asFloat());
			}
		}

		#pragma endregion
		
		#pragma region ADD SOUND INTO MAP

		//check if music exist then play it
		auto audioEvent = this->mapInformation->getObjectGroup("Events")->getObject("SOUND");
		const char* musicName = audioEvent.at("music").asString().c_str();
		if (musicName != "")
		{
			auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
			// set the background music and continuously play it.
			audio->playBackgroundMusic("sound/music/route29.mp3", true);
		}

		#pragma endregion

		//finally add it
		this->addChild(this->mapInformation, 0, "map_information");
		layer->addChild(this, 0, "map");
		MapManager::mapInfo = this->mapInformation;

	}
}

cocos2d::TMXTiledMap* MapManager::getMapInformation()
{
	return this->mapInformation;
}

bool MapManager::getCollisionForCharacter(class Character* chara)
{

	float screeWidth = (MapManager::getMapInfo()->getMapSize().width * MapManager::getMapInfo()->getTileSize().width);
	float screeHeight = (MapManager::getMapInfo()->getMapSize().height * MapManager::getMapInfo()->getTileSize().height);

	switch (chara->getDirection())
	{
		case TOP:
			chara->getCollisionBox()->setRect(chara->getPositionX(), chara->getPositionY() + 1, 1, 1);
			//if the player is at the border of the windows 
			if (chara->getPositionY() >= (screeHeight + 20))
				return true;
		break;
		case DOWN:
			chara->getCollisionBox()->setRect(chara->getPositionX(), chara->getPositionY() - 1, 1, 2);
			//if the player is at the border of the windows 
			if (chara->getPositionY() <= 10)
				return true;
		break;
		case RIGHT:
			chara->getCollisionBox()->setRect(chara->getPositionX() + 3, chara->getPositionY(), 1, 0);
			//if the player is at the border of the windows 
			if (chara->getPositionX() >= (screeWidth - 10))
				return true;
		break;
		case LEFT:
			chara->getCollisionBox()->setRect(chara->getPositionX() - 3, chara->getPositionY(), 1, 0);
			//if the player is at the border of the windows 
			if (chara->getPositionX() <= 10)
				return true;
		break;
	}

	//check if the character is hit a collision box
	bool hasCollision = false;
	for (auto collision : MapManager::getCollisions())
	{
		if (chara->getCollisionBox()->intersectsRect(Rect(collision.x, collision.y, collision.width, collision.height)))
		{
			hasCollision = true;
			break;
		}
		else
			hasCollision = false;
	}

	return hasCollision;
}

bool MapManager::checkFightForCharacter(class Trainer* chara)
{
	//check if the character is hit a collision box
	bool hasFighting = false;
	for (auto collision : MapManager::getFightZone())
	{
		if (chara->getCollisionBox()->intersectsRect(Rect(collision.x, collision.y, collision.width, collision.height)))
		{

			if (random((Trainer::getFMaxFootstep() / 2), Trainer::getFMaxFootstep()) < chara->getFootstep())
			{
				hasFighting = true;
				break;
			}
		}
		else
			hasFighting = false;
	}

	return hasFighting;
}


TMXTiledMap* MapManager::getMapInfo()
{
	return MapManager::mapInfo;
}

vector<struct Collision> MapManager::getCollisions()
{
	return MapManager::collisions;
}

vector<struct Collision> MapManager::getFightZone()
{
	return MapManager::fightZone;
}

void MapManager::addPlayerToTheMap(Character* character)
{
	MapManager::getMapInfo()->addChild(character,TILE_PLAYER, "trainer");
	
}
